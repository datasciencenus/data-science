function initMap() {
  var mapDiv = document.getElementById('map');
  var map = new google.maps.Map(mapDiv, {
    center: { lat: 1.35, lng: 103.851959 },
    zoom: 11
  });

  var ageAreas = {};

  var schools = [];

  var schoolAreas = {};
  var parkAreas = {};
  var priceAreas = {};

  var kmlUrl = 'http://paradite.com/boundary18.kml';
  var kmlOptions = {
    suppressInfoWindows: true,
    preserveViewport: true,
    map: map,
    zIndex: 1
  };
  var kmlLayer = new google.maps.KmlLayer(kmlUrl, kmlOptions);

  // parks
  var kmlParkUrl = 'http://paradite.com/RelaxSG4.kml';
  var kmlParkOptions = {
    suppressInfoWindows: true,
    preserveViewport: true,
    zIndex: 2
  };
  var kmlParkLayer = new google.maps.KmlLayer(kmlParkUrl, kmlParkOptions);

  var infoWindow = new google.maps.InfoWindow();

  // schools
  // http://webdesigningdevelopment.com/learning/wp-content/uploads/2015/10/31kmog2BHQL1.png
  var image = {
    url: 'http://paradite.com/edus.png',
    origin: new google.maps.Point(0, 0),
    anchor: new google.maps.Point(16, 16)
  };
  // 103.816250268609,1.31104847734541

  d3.csv('school.csv', function(d) {
    return {
      name: d.name,
      lat: +d.lat,
      lng: +d.lon,
      town: d.town
    };
  }, function(err, data) {
    if(err) {
      console.log(err);
    }
    // console.log(data);
    schools = data;
    for (var i = 0; i < schools.length; i++) {

      schools[i].marker = new google.maps.Marker({
        position: {lat: schools[i].lat, lng: schools[i].lng},
        icon: image,
        name: schools[i].name
      });

      schools[i].marker.addListener('click', function(i) {
        return function() {
          // console.log(schoolMarker);
          infoWindow.close();
          infoWindow.setOptions({
            content: getOverlayContent(schools[i].marker),
          });
          infoWindow.open(map, schools[i].marker);
        }
      }(i));
    }
  });

  // var schools = [{lat: 1.31204847734541, lng: 103.816250268609, name: 'new school'}];
  // 
  // age
  // 
  var proxy = 'http://localhost:3000';
  var url = "https://ussouthcentral.services.azureml.net/workspaces/604b65644f9d4f19942387ba68533743/services/6e440cc37f074c5abc9a22011d9b9956/execute?api-version=2.0&details=true";

  var newUrl = url.replace('https://ussouthcentral.services.azureml.net', proxy);

  // d3.json(newUrl)
  d3.json('age_areas.json')
  .header('Content-Type', 'application/json')
  .get(function(err, data) {
    if (err) {
      console.log(err);
    } else {
      // console.log(JSON.stringify(data, null, 2));
      var areas = data.Results['minor & retired'].value.Values;
      for (var i = 0; i < areas.length; i++) {
        ageAreas[areas[i][0]] = areas[i];
      }
    }
  });

  // aggr park and school
  d3.csv('park_and_school_by_town.csv', function(d) {
    return {
      type: d.type,
      count: +d.count,
      town: d.town
    };
  }, function(err, data) {
    if (err) {
      console.log(err);
    } else {
      for (var i = 0; i < data.length; i++) {
        // console.log(data[i]);
        if(data[i].type === 'Park') {
          parkAreas[data[i].town] = data[i].count;
        } else if(data[i].type === 'School') {
          schoolAreas[data[i].town] = data[i].count;
        }
      }
    }
  });

  // resale price
  d3.csv('resale_prices_new.csv', function(d) {
    return {
      town: captMultiPart(d.id),
      array: [[2013, +d['2013']], [2014, +d['2014']], [2015, +d['2015']], [2016, +d['2016']], [2017, +d['2017']]]
    };
  }, function(err, data) {
    if (err) {
      console.log(err);
    } else {
      for (var i = 0; i < data.length; i++) {
        priceAreas[data[i].town] = data[i].array;
      }
      console.log(priceAreas);
    }
  });

  function captMultiPart(string) {
    return string.split(' ').map(function(s) {return capitalise(s);}).join(' ');
  }

  function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
  }

  function handleZoom(event) {
    if(map.getZoom() >= 14) {
      for (var i = 0; i < schools.length; i++) {
        schools[i].marker.setMap(map);
      }
      kmlParkLayer.setMap(map);
    } else {
      kmlParkLayer.setMap(null);
      for (var i = 0; i < schools.length; i++) {
        schools[i].marker.setMap(null);
      }
    }
  }

  google.maps.event.addListener(map, 'zoom_changed', handleZoom);

  // chart

  var margin = { top: 25, right: 0, bottom: 30, left: 50 },
    width = 300 - margin.left - margin.right,
    height = 100 - margin.top - margin.bottom;

  var x = d3.scale.linear()
    .range([0, width - 30]);

  var y = d3.scale.linear()
    .range([height, 0]);

  var xAxis = d3.svg.axis()
    .scale(x)
    .tickValues([2013, 2014, 2015, 2016, 2017])
    .tickFormat(d3.format("d"))
    .orient("bottom");

  var yAxis = d3.svg.axis()
    .scale(y)
    .ticks(3)
    .orient("left");

  var line = d3.svg.line()
    .x(function(d) {
      return x(d[0]);
    })
    .y(function(d) {
      return y(d[1]);
    });

  function refreshChart(area) {
    var youngPer = 'N.A.';
    var midPer = 'N.A.';
    var oldPer = 'N.A.';
    if(ageAreas[area] && ageAreas[area][1]) {
      youngPer = +((+ageAreas[area][1]) * 100).toFixed(1);
      midPer = +((+ageAreas[area][2]) * 100).toFixed(1);
      oldPer = +((+ageAreas[area][3]) * 100).toFixed(1);
    }

    var parkNo = 'N.A.';
    var schoolNo = 'N.A.';
    // console.log(parkAreas);
    if(parkAreas[area] != undefined) {
      parkNo = parkAreas[area] + ' parks';
    }

    if(schoolAreas[area] != undefined) {
      schoolNo = schoolAreas[area] + ' schools';
    }

    var chart = d3.select("#chart")

    var iconWrapper = chart.append('div')
      .classed("icon-wrapper", true);

    var schoolArea = iconWrapper.append('div')
      .classed("icon-area", true);

    schoolArea.append('div')
      .html('<i class="fa fa-graduation-cap fa-4x" style="color:blue;"></i>');

    schoolArea.append('div')
      .classed('icon-text', true)
      .text(schoolNo);

    var parkArea = iconWrapper.append('div')
      .classed("icon-area", true);

    parkArea.append('div')
      .classed('scaleup', true)
      .html('<img src="tree.png"></img>');

    parkArea.append('div')
      .classed('icon-text', true)
      .text(parkNo);

    var youngArea = iconWrapper.append('div')
      .classed("icon-area", true);

    youngArea.append('div')
      .html('<img src="young-copy.png"></img>');

    youngArea.append('div')
      .classed('icon-text', true)
      .text(youngPer + '%');
    
    // var midArea = iconWrapper.append('div')
    //   .classed("icon-area", true);

    // midArea.append('div')
    //   .html('<img src="/people.png"></img>');

    // midArea.append('div')
    //   .classed('icon-text', true)
    //   .text(midPer + '%');

    var oldArea = iconWrapper.append('div')
      .classed("icon-area", true);

    oldArea.append('div')
      .html('<img src="elderly-copy.png"></img>');

    oldArea.append('div')
      .classed('icon-text', true)
      .text(oldPer + '%');    


    var priceData = null;
    
    if(!priceAreas[area]) {
      d3.select('.container').style("height", "140px");

      chart.append('div')
      .classed('subtitle', true)
      .classed('vert-center', true)
      .text('HDB resale price not available.');
      return;
    }

    priceData = priceAreas[area];

    chart.append('div')
      .classed('subtitle', true)
      .text('HDB resale price');

    var svg = chart.append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    x.domain(d3.extent(priceData, function(d) {
      return d[0]; }));

    var yExtent = d3.extent(priceData, function(d) {
      return d[1]; });
    // console.log(yExtent);
    y.domain([yExtent[0] * 0.95, yExtent[1] * 1.05]);

    svg.append("g")
      .attr("class", "x axis")
      .attr("transform", "translate(0," + height + ")")
      .call(xAxis);

    svg.append("g")
      .attr("class", "y axis")
      .call(yAxis)
      .append("text")
      // .attr("transform", "rotate(-90)")
      .attr("x", 8)
      .attr("y", 0)
      .attr("dy", "-.81em")
      .style("text-anchor", "end")
      .text("Price (S$)");

    var first = priceData.slice(0, priceData.length - 1);
    var second = [priceData[priceData.length - 2], priceData[priceData.length - 1]];
    console.log(first);
    console.log(second);

    svg.append("path")
      .datum(first)
      .attr("class", "line")
      .attr("d", line);

    svg.append("path")
      .datum(second)
      .attr("class", "line predicted")
      .attr("stroke-dasharray", "5,5")
      .transition(200)
      .attr("d", line);
  }

  function getOverlayContentCard(featureData) {
    return '<div class="container" style="font-family: Roboto,Arial,sans-serif; font-size: small"><div class="title-text">' + featureData.name + '</div>' + '<div id="chart"></div>';
  }

  function getOverlayContent(featureData) {
    return '<div style="font-family: Roboto,Arial,sans-serif; font-size: small"><div class="title-text">' + featureData.name + '</div>';
  }

  function openIWCard(KMLevent) {
    infoWindow.close();
    infoWindow.setOptions({
      content: getOverlayContentCard(KMLevent.featureData),
      position: KMLevent.latLng,
      pixelOffset: KMLevent.pixelOffset
    });
    infoWindow.open(map);
    setTimeout(function(){ refreshChart(KMLevent.featureData.name); }, 0);
  }

  function openIW(KMLevent) {
    infoWindow.close();
    infoWindow.setOptions({
      content: getOverlayContent(KMLevent.featureData),
      position: KMLevent.latLng,
      pixelOffset: KMLevent.pixelOffset
    });
    infoWindow.open(map);
  }

  google.maps.event.addListener(kmlLayer, 'click', openIWCard);

  google.maps.event.addListener(kmlParkLayer, 'click', openIW);
}